#!/usr/bin/env bash

composer install
npm install

php bin/console c:c --env=dev
php bin/console c:c --env=prod