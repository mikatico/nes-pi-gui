// eslint-disable

const path = require('path');
const node_modules = path.resolve(__dirname, './node_modules');

module.exports = (grunt) => {
  // Project configuration.
  grunt.initConfig({
    sass: {                              // Task 
      dist: {                            // Target 
        options: {                       // Target options 
          style: 'expanded'
        },
        files: {                         // Dictionary of files 
          'web/v1/css/main.css': 'web/v1/scss/main.scss'
        }
      }
    },
    browserify: {
      dist: {
        options: {
           transform: [['babelify', {presets: ['es2015']}]]
        },        
        src: ['web/v1/es6/app.js'],
        dest: 'web/v1/js/app.js'
      }
    }
  });

  grunt.loadNpmTasks('grunt-browserify'); 
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.registerTask('default', ['browserify']);
};
