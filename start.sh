#!/usr/bin/env bash

php bin/console s:r 127.0.0.1:2507 -p prod &
chromium-browser --kiosk http://127.0.0.1:2507/app.php
