<?php

namespace NesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $games = $this->getParameter('games');
        // replace this example code with whatever you need
        return $this->render('@Nes/nes/index.html.twig', [
            'games' => $games
        ]);
    }

    /**
     * @param $game
     *
     * @Route("/play/{game}")
     *
     * @return Response
     */
    public function playGameAction(string $game)
    {
        $gamePath = $this->get('kernel')->getRomsDir() . '/' . $game;
        if (!file_exists($gamePath)) {
            return new JsonResponse([
                'status' => 'error',
                'message' => 'Game not found !'
            ], Response::HTTP_NOT_FOUND);
        }

        $process = new Process(sprintf('php %s/bin/console nes:game:play %s -e prod', $this->get('kernel')->getAppDir(), $gamePath));

        try {
            $process->run();

            return new JsonResponse([
                'status' => 'ok',
                'message' => 'Good game !'
            ], Response::HTTP_OK);
        } catch (ProcessFailedException $e) {
            return new JsonResponse([
                'status' => 'error',
                'message' => 'The process has not worked !'
            ], Response::HTTP_BAD_GATEWAY);
        }

    }
}
