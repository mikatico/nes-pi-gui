<?php

namespace NesBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class NesPlayGameCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nes:game:play')
            ->addArgument('game', InputArgument::REQUIRED, 'Path to game')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $game = $input->getArgument('game');

        if (!file_exists($game)) {
            throw new \Exception('Invalid game');
        }

        $options = implode(' ', $this->getContainer()->getParameter('emulator_config'));

        $emulatorPath = $this->getContainer()->getParameter('emulator_path');
        if ($emulatorPath) {
            $emulatorPath = $this->getContainer()->get('kernel')->getAppDir() . $emulatorPath .'fceux';
        } else {
            $emulatorPath = 'fceux';
        }

        $process = new Process(sprintf('%s %s %s &', $emulatorPath, $options, $game));

        $process->run();

        $output->writeln('Command result.');
    }

}
