nes-pi-gui
==========

# Installation

## NodeJS
Install NodeJS by either :

- Downloading it and installing it from [official node js website](https://nodejs.org/en/download/)
- Using a version manager if you need to manage multiple version - [nvm | GitHub](https://github.com/creationix/nvm#install-script) 

### NPM
By default, `npm` is built in node when you install it.  
Optionally, you can update it with the following command:
```bash
npm i -g npm
```

