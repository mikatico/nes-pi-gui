(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

require('./ui/slide');

require('./config/constants');

var _nespi = require('./nespi');

var _nespi2 = _interopRequireDefault(_nespi);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

document.addEventListener('DOMContentLoaded', function () {
  var nes = new _nespi2.default();

  window.nes = nes;
});

},{"./config/constants":2,"./nespi":8,"./ui/slide":9}],2:[function(require,module,exports){
'use strict';

window.Const = {};

window.Const.UP = 'direction.up';
window.Const.DOWN = 'direction.down';
window.Const.LEFT = 'direction.left';
window.Const.RIGHT = 'direction.right';

window.Const.SELECT = 8;
window.Const.START = 9;

window.Const.A = 1;
window.Const.B = 2;

},{}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.hasClass = hasClass;
exports.addClass = addClass;
exports.removeClass = removeClass;
exports.toggleClass = toggleClass;
var classList = document.documentElement.classList;

function hasClass(elem, className) {
  if (classList) {
    return elem.classList.contains(className);
  }
  return new RegExp('(^|\\s)' + className + '(\\s|$)').test(elem.className);
}

function addClass(elem, className) {
  if (!this.hasClass(elem, className)) {
    if (classList) {
      elem.classList.add(className);
    } else {
      elem.className += (elem.className ? ' ' : '') + className;
    }
  }
}

function removeClass(elem, className) {
  if (this.hasClass(elem, className)) {
    if (classList) {
      elem.classList.remove(className);
    } else {
      elem.className = elem.className.replace(new RegExp('(^|\\s)*' + className + '(\\s|$)*', 'g'), '');
    }
  }
}

function toggleClass(elem, className) {
  if (classList) {
    elem.classList.toggle(className);
  } else {
    if (this.hasClass(elem, className)) {
      // eslint-disable-line
      elem.removeClass(className);
    } else {
      elem.addClass(className);
    }
  }
}

},{}],4:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var client = {
  create_request: function create_request(method, url) {
    var data = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

    var object = {
      method: method,
      body: data,
      header: {
        'X-Requested-With': 'XMLHttpRequest'
      },
      credentials: 'same-origin'
    };

    return fetch(url, object);
  },
  get: function get(url, callback) {
    this.create_request('GET', url).then(function (response) {
      return response.json();
    }).then(callback);
  },
  post: function post(url, data, callback) {
    this.create_request('GET', url, data).then(function (response) {
      return response.json();
    }).then(callback);
  }
};

window.client = client || {};

exports.default = client;

},{}],5:[function(require,module,exports){
'use strict';

var controllers = {};
var rAF = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame;

var currentSeconrd;

var lastCalledTime;
var fps;

function getFps() {

  var d = new Date();

  if (!lastCalledTime) {
    lastCalledTime = Date.now();
    currentSeconrd = d.getSeconds();
    fps = 0;
    document.getElementById('fps').innerHTML = fps + ' fps';
  }
  var delta = (Date.now() - lastCalledTime) / 1000;
  lastCalledTime = Date.now();
  fps = Math.round(1 / delta);

  if (d.getSeconds() !== currentSeconrd) {
    document.getElementById('fps').innerHTML = fps + ' fps';
    currentSeconrd = d.getSeconds();
  }
}

var axesHorizontalPressed = false;
var axesVerticalPressed = false;

var actionEnableA = true;
var actionEnableB = true;
var actionEnableSTART = true;
var actionEnableSELECT = true;

function scangamepads() {
  var gamepads = navigator.getGamepads ? navigator.getGamepads() : navigator.webkitGetGamepads ? navigator.webkitGetGamepads() : []; // eslint-disable-line
  for (var i = 0; i < gamepads.length; i += 1) {
    if (gamepads[i]) {
      if (!(gamepads[i].index in controllers)) {
        addgamepad(gamepads[i]);
      } else {
        controllers[gamepads[i].index] = gamepads[i];
      }
    }
  }
}

function addgamepad(gamepad) {
  controllers[gamepad.index] = gamepad;
  // rAF(updateStatus);
}

function updateStatus() {
  getFps();
  if (!document.hasFocus()) {
    rAF(updateStatus);
    return 0;
  }
  scangamepads();
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = Object.keys(controllers)[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var j = _step.value;

      var controller = controllers[j];

      var A = controller.buttons[Const.A];
      var B = controller.buttons[Const.B];

      var START = controller.buttons[Const.START];
      var SELECT = controller.buttons[Const.SELECT];

      if (A.pressed) {
        if (actionEnableA) {
          nes.action(Const.A);
          actionEnableA = false;
        }
      } else {
        actionEnableA = true;
      }

      if (B.pressed) {
        if (actionEnableB) {
          nes.action(Const.B);
          actionEnableB = false;
        }
      } else {
        actionEnableB = true;
      }

      if (START.pressed) {
        if (actionEnableSTART) {
          nes.action(Const.START);
          actionEnableSTART = false;
        }
      } else {
        actionEnableSTART = true;
      }

      if (SELECT.pressed) {
        if (actionEnableSELECT) {
          nes.action(Const.SELECT);
          actionEnableSELECT = false;
        }
      } else {
        actionEnableSELECT = true;
      }

      if (Math.round(controller.axes[0]) === 0 && axesHorizontalPressed) {
        axesHorizontalPressed = false;
      }

      if (Math.round(controller.axes[1]) === 0 && axesVerticalPressed) {
        axesVerticalPressed = false;
      }

      if (controller.axes[0] === 1) {
        if (!axesHorizontalPressed) {
          axesHorizontalPressed = true;
          if (nes !== undefined) {
            nes.move(Const.RIGHT);
          }
        }
      } else if (controller.axes[0] === -1) {
        if (!axesHorizontalPressed) {
          axesHorizontalPressed = true;
          if (nes !== undefined) {
            nes.move(Const.LEFT);
          }
        }
      }

      if (controller.axes[1] === 1) {
        if (!axesVerticalPressed) {
          axesVerticalPressed = true;
          if (nes !== undefined) {
            nes.move(Const.DOWN);
          }
        }
      } else if (controller.axes[1] === -1) {
        if (!axesVerticalPressed) {
          axesVerticalPressed = true;
          if (nes !== undefined) {
            nes.move(Const.UP);
          }
        }
      }
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  rAF(updateStatus);
}

updateStatus();

},{}],6:[function(require,module,exports){
'use strict';

document.addEventListener('keydown', function (event) {
  var keyName = event.key;

  if (keyName === 'ArrowLeft') {}

  if (keyName === 'ArrowRight') {}
}, false);

},{}],7:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _classes = require('../dom/classes');

var C = _interopRequireWildcard(_classes);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Navigation = function () {
  function Navigation(x, y) {
    _classCallCheck(this, Navigation);

    this.modal = false;
    var profil = document.querySelector('#profil');
    var card = document.querySelector('.owl-item.active.center');
    var game = card.firstChild;
    var parameters = document.querySelector('#parameters');
    var shudown = document.querySelector('#shutdown');

    C.addClass(game, 'focus');

    var header = [];
    header.push(profil);

    var body = [];
    body.push(game);

    var footer = [];
    footer.push(parameters);
    footer.push(shudown);

    this.tree = [];
    this.tree.push(header);
    this.tree.push(body);
    this.tree.push(footer);

    this.x = x;
    this.y = y;
  }

  _createClass(Navigation, [{
    key: 'getX',
    value: function getX() {
      return this.x;
    }
  }, {
    key: 'setX',
    value: function setX(x) {
      this.x = x;
    }
  }, {
    key: 'getY',
    value: function getY() {
      return this.y;
    }
  }, {
    key: 'setY',
    value: function setY(y) {
      this.y = y;
    }
  }, {
    key: 'getCurrent',
    value: function getCurrent() {
      return this.tree[this.y][this.x];
    }
  }, {
    key: 'getTree',
    value: function getTree() {
      return this.tree;
    }
  }, {
    key: 'increment',
    value: function increment(value) {
      return value + 1;
    }
  }, {
    key: 'decrement',
    value: function decrement(value) {
      return value - 1;
    }
  }, {
    key: 'getCurrentPositionX',
    value: function getCurrentPositionX() {
      return this.getX();
    }
  }, {
    key: 'getCurrentPositionY',
    value: function getCurrentPositionY() {
      return this.getY();
    }
  }, {
    key: 'nextX',
    value: function nextX() {
      if (!this.modal) {
        var oldItem = this.getCurrent();
        this.setX(this.increment(this.x));
        var newItem = this.getCurrent();
        C.removeClass(oldItem, 'focus');
        C.addClass(newItem, 'focus');
      }
    }
  }, {
    key: 'prevX',
    value: function prevX() {
      if (!this.modal) {
        var oldItem = this.getCurrent();
        this.setX(this.decrement(this.x));
        var newItem = this.getCurrent();
        C.removeClass(oldItem, 'focus');
        C.addClass(newItem, 'focus');
      }
    }
  }, {
    key: 'nextY',
    value: function nextY() {
      if (!this.modal) {
        var oldItem = this.getCurrent();
        this.setX(0);
        this.setY(this.increment(this.y));
        var newItem = this.getCurrent();
        C.removeClass(oldItem, 'focus');
        C.addClass(newItem, 'focus');
      }
    }
  }, {
    key: 'prevY',
    value: function prevY() {
      if (!this.modal) {
        var oldItem = this.getCurrent();
        this.setX(0);
        this.setY(this.decrement(this.y));
        var newItem = this.getCurrent();
        C.removeClass(oldItem, 'focus');
        C.addClass(newItem, 'focus');
      }
    }
  }, {
    key: 'focusCard',
    value: function focusCard() {
      C.addClass(this.tree[1][0], 'focus');
    }
  }, {
    key: 'refreshCard',
    value: function refreshCard() {
      var last = this.getCurrent();
      C.removeClass(last, 'focus');
      var card = document.querySelector('.owl-item.active.center');
      var game = card.firstChild;
      C.addClass(game, 'focus');
      this.tree[1][0] = game;
    }
  }]);

  return Navigation;
}();

exports.default = Navigation;

},{"../dom/classes":3}],8:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _classes = require('./dom/classes');

var C = _interopRequireWildcard(_classes);

var _navigation = require('./helper/navigation');

var _navigation2 = _interopRequireDefault(_navigation);

var _client = require('./helper/client');

var _client2 = _interopRequireDefault(_client);

require('./helper/keyboard');

require('./helper/gamepad');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

window.Const = Const;

var Nespi = function () {
  function Nespi() {
    _classCallCheck(this, Nespi);

    this.applicationName = 'NES PI';
    this.navigation = new _navigation2.default(0, 1);
    this.client = _client2.default;
    this.btnNext = document.querySelector('.next');
    this.btnPrev = document.querySelector('.prev');

    this.initializeEvent();
  }

  _createClass(Nespi, [{
    key: 'initializeEvent',
    value: function initializeEvent() {
      var _this = this;

      this.btnNext.addEventListener('click', function () {
        _this.slideNext();
      });

      this.btnPrev.addEventListener('click', function () {
        _this.slidePrev();
      });
    }
  }, {
    key: 'getName',
    value: function getName() {
      return this.applicationName;
    }
  }, {
    key: 'getNavigation',
    value: function getNavigation() {
      return this.navigation;
    }
  }, {
    key: 'move',
    value: function move(direction) {
      var currentPositionY = this.navigation.getCurrentPositionY();
      var currentPositionX = this.navigation.getCurrentPositionX();
      switch (direction) {
        case Const.UP:
          switch (currentPositionY) {
            case 0:
              break;
            case 1:
              this.navigation.prevY();
              break;
            case 2:
              this.navigation.prevY();
              break;
            default:
              break;
          }
          break;
        case Const.DOWN:
          switch (currentPositionY) {
            case 0:
              this.navigation.nextY();
              break;
            case 1:
              this.navigation.nextY();
              break;
            case 2:
              break;
            default:
              break;
          }
          break;
        case Const.LEFT:
          switch (currentPositionY) {
            case 0:
              break;
            case 1:
              this.slidePrev();
              this.navigation.refreshCard();
              break;
            case 2:
              if (currentPositionX == 1) {
                this.navigation.prevX();
              }
              break;
            default:
              break;
          }
          break;
        case Const.RIGHT:
          switch (currentPositionY) {
            case 0:
              break;
            case 1:
              this.slideNext();
              this.navigation.refreshCard();
              this.navigation.focusCard();
              break;
            case 2:
              if (currentPositionX == 0) {
                this.navigation.nextX();
              }
              break;
            default:
              break;
          }
          break;
        default:
          break;
      }
    }
  }, {
    key: 'action',
    value: function action(btn) {
      console.log('click ' + btn);
      switch (btn) {
        case Const.START:
          console.log(btn);
          break;
        case Const.SELECT:
          console.log(btn);
          break;
        case Const.A:
          var current = this.navigation.getCurrent();
          this.execute(current);
          break;
        case Const.B:
          console.log(btn);
          break;
        default:
          console.log(btn);
          break;
      }
    }
  }, {
    key: 'execute',
    value: function execute(obj) {
      // var modal = document.getElementById('myModal');
      // modal.style.display = "block";
      var action = obj.getAttribute('action');
      switch (action) {
        case 'start-game':
          var rom = obj.getAttribute('rom'); // TODO MAKE A MODAL CONFIRM
          this.client.get('/v1/play/' + rom, function (response) {
            // TODO case ERROR FOR MESSAGE ALERT 
          });
          break;
        case 'show-profil':

          break;
        case 'show-parameters':

          break;
        case 'show-power-action':

          break;
        default:
          break;
      }
    }
  }, {
    key: 'slideNext',
    value: function slideNext() {
      slide.trigger('next.owl.carousel');
    }
  }, {
    key: 'slidePrev',
    value: function slidePrev() {
      slide.trigger('prev.owl.carousel', [300]);
    }
  }]);

  return Nespi;
}();

exports.default = Nespi;

},{"./dom/classes":3,"./helper/client":4,"./helper/gamepad":5,"./helper/keyboard":6,"./helper/navigation":7}],9:[function(require,module,exports){
'use strict';

var slide = $('.slide');

slide.owlCarousel({
    item: 3,
    center: true,
    loop: true,
    nav: false,
    dots: true,
    autoplay: false,
    autoWidth: true,
    rewindNav: false,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 3
        },
        1000: {
            items: 5
        }
    }
});

window.slide = slide;

},{}]},{},[1]);
