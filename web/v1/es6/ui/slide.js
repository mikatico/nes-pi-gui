const slide = $('.slide');

slide.owlCarousel({
    item: 3,
    center: true,
    loop: true,
    nav: false,
    dots: true,
    autoplay: false,
    autoWidth: true,
    rewindNav: false,
    responsive:{
      0:{
          items:1
      },
      600:{
          items:3
      },
      1000:{
          items:5
      }
  }
});

window.slide = slide;