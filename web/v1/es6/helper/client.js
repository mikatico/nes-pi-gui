const client = {

  create_request(method, url, data = {}) {
    const object = {
      method: method,
      body: data,
      header: {
        'X-Requested-With': 'XMLHttpRequest'
      },
      credentials: 'same-origin'
    };

    return fetch(url, object);
  },

  get(url, callback) {
    this.create_request('GET', url).then((response) => {
      return response.json();
    }).then(callback);
  },

  post(url, data, callback) {
    this.create_request('GET', url, data).then((response) => {
      return response.json();
    }).then(callback);
  }
};

window.client = client || {};

export default client;
