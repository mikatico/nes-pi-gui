const controllers = {};
const rAF = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame;

var currentSeconrd;

var lastCalledTime;
var fps;

function getFps() {

  let d = new Date();

  if(!lastCalledTime) {
     lastCalledTime = Date.now();
     currentSeconrd = d.getSeconds();
     fps = 0;
     document.getElementById('fps').innerHTML = fps + ' fps';
  }
  var delta = (Date.now() - lastCalledTime)/1000;
  lastCalledTime = Date.now();
  fps = Math.round(1/delta);

  if (d.getSeconds() !== currentSeconrd) {
    document.getElementById('fps').innerHTML = fps + ' fps';
    currentSeconrd = d.getSeconds();
  }
} 

let axesHorizontalPressed = false;
let axesVerticalPressed = false;

let actionEnableA = true;
let actionEnableB = true;
let actionEnableSTART = true;
let actionEnableSELECT = true;

function scangamepads() {
  const gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads() : []); // eslint-disable-line
  for (let i = 0; i < gamepads.length; i += 1) {
    if (gamepads[i]) {
      if (!(gamepads[i].index in controllers)) {
        addgamepad(gamepads[i]);
      }
      else {
        controllers[gamepads[i].index] = gamepads[i];
      }
    }
  }
}

function addgamepad(gamepad) {
  controllers[gamepad.index] = gamepad;
  // rAF(updateStatus);
}


function updateStatus() {
  getFps();
  if (!document.hasFocus()) {
    rAF(updateStatus);
    return 0;
  }
  scangamepads();
  for (const j of Object.keys(controllers)) {
    const controller = controllers[j];

    const A = controller.buttons[Const.A];
    const B = controller.buttons[Const.B];

    const START = controller.buttons[Const.START];
    const SELECT = controller.buttons[Const.SELECT];

    if (A.pressed) {
      if (actionEnableA) {
          nes.action(Const.A);
          actionEnableA = false;
        }
    } else {
      actionEnableA = true;
    }

    if (B.pressed) {
      if (actionEnableB) {
          nes.action(Const.B);
          actionEnableB = false;
        }
    } else {
      actionEnableB = true;
    }

    if (START.pressed) {
      if (actionEnableSTART) {
          nes.action(Const.START);
          actionEnableSTART = false;
        }
    } else {
      actionEnableSTART = true;
    }

    if (SELECT.pressed) {
      if (actionEnableSELECT) {
          nes.action(Const.SELECT);
          actionEnableSELECT = false;
        }
    } else {
      actionEnableSELECT = true;
    }

    if (Math.round(controller.axes[0]) === 0 && axesHorizontalPressed) {
      axesHorizontalPressed = false;
    }

    if (Math.round(controller.axes[1]) === 0 && axesVerticalPressed) {
      axesVerticalPressed = false;
    }

    if (controller.axes[0] === 1) {
      if (!axesHorizontalPressed) {
        axesHorizontalPressed = true;
        if (nes !== undefined) {
          nes.move(Const.RIGHT);
        }
      }
    }
    else if (controller.axes[0] === -1) {
      if (!axesHorizontalPressed) {
        axesHorizontalPressed = true;
        if (nes !== undefined) {
          nes.move(Const.LEFT);
        }
      }
    }

    if (controller.axes[1] === 1) {
      if (!axesVerticalPressed) {
        axesVerticalPressed = true;
        if (nes !== undefined) {
          nes.move(Const.DOWN);
        }
      }
    }
    else if (controller.axes[1] === -1) {
      if (!axesVerticalPressed) {
        axesVerticalPressed = true;
        if (nes !== undefined) {
          nes.move(Const.UP);
        }
      }
    }
  }
  rAF(updateStatus);
}

updateStatus();