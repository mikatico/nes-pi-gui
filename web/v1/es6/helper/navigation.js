import * as C from '../dom/classes';

class Navigation {
    constructor(x, y) {
      this.modal = false;
      const profil = document.querySelector('#profil');
      const card = document.querySelector('.owl-item.active.center');
      const game = card.firstChild;
      const parameters = document.querySelector('#parameters');
      const shudown = document.querySelector('#shutdown');

      C.addClass(game, 'focus');

      const header = [];
      header.push(profil);

      const body = [];
      body.push(game);

      const footer = [];
      footer.push(parameters);
      footer.push(shudown);

      this.tree = [];
      this.tree.push(header);
      this.tree.push(body);
      this.tree.push(footer);

      this.x = x;
      this.y = y;
    }

    getX() {
      return this.x;
    }

    setX(x) {
      this.x = x;
    }

    getY() {
      return this.y;
    }

    setY(y) {
      this.y = y;
    }

    getCurrent() {
      return this.tree[this.y][this.x];
    }

    getTree() {
      return this.tree;
    }

    increment(value) {
      return value + 1;
    }

    decrement(value) {
      return value - 1;
    }
    
    getCurrentPositionX() {
      return this.getX();
    }

    getCurrentPositionY() {
      return this.getY();
    }

    nextX() {
      if (!this.modal) {
        let oldItem = this.getCurrent();
        this.setX(this.increment(this.x));
        let newItem = this.getCurrent();
        C.removeClass(oldItem, 'focus');
        C.addClass(newItem, 'focus');
      } 
    }

    prevX() {
      if (!this.modal) {
        let oldItem = this.getCurrent();
        this.setX(this.decrement(this.x));
        let newItem = this.getCurrent();
        C.removeClass(oldItem, 'focus');
        C.addClass(newItem, 'focus');
      }
    }

    nextY() {
      if (!this.modal) {
        let oldItem = this.getCurrent();
        this.setX(0);
        this.setY(this.increment(this.y));
        let newItem = this.getCurrent();
        C.removeClass(oldItem, 'focus');
        C.addClass(newItem, 'focus');
      }
    }

    prevY() {
      if (!this.modal) {
        let oldItem = this.getCurrent();
        this.setX(0);
        this.setY(this.decrement(this.y));
        let newItem = this.getCurrent();
        C.removeClass(oldItem, 'focus');
        C.addClass(newItem, 'focus');
      }
    }

    focusCard() {
      C.addClass(this.tree[1][0], 'focus');
    }

    refreshCard() {
      const last = this.getCurrent();
      C.removeClass(last, 'focus');
      const card = document.querySelector('.owl-item.active.center');
      const game = card.firstChild;
      C.addClass(game, 'focus');
      this.tree[1][0] = game;
    }

}

export default Navigation;
