window.Const = {};

window.Const.UP = 'direction.up';
window.Const.DOWN = 'direction.down';
window.Const.LEFT = 'direction.left';
window.Const.RIGHT = 'direction.right';

window.Const.SELECT = 8;
window.Const.START = 9;

window.Const.A = 1;
window.Const.B = 2;
