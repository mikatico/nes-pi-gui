/**
 * @module helpers
 * All the helper functions needed in this project
 */
export default  {
  createElem: (el, className) => {
    const elem = document.createElement(el);
    elem.className = className;
    return elem;
  },

  animationEvent: () => {
    const el = document.createElement('_');
    const animations = {
      'animation': 'animationend',
      'OAnimation': 'oAnimationEnd',
      'MozAnimation': 'animationend',
      'WebkitAnimation': 'webkitAnimationEnd'
    };
    /* eslint-disable */
    for (const t in animations) {
      if (el.style[t] !== undefined) {
        return animations[t];
      }
    }
  },

  vendorPrefix: (cssprop) => {
    var css3vendors = ['', '-moz-','-webkit-','-o-','-ms-','-khtml-']
    var root = document.documentElement
    function camelCase(str){
      return str.replace(/\-([a-z])/gi, function (match, p1){ // p1 references submatch in parentheses
        return p1.toUpperCase() // convert first letter after "-" to uppercase
      })
    }
    for (var i=0; i<css3vendors.length; i++){
      var css3propcamel = camelCase( css3vendors[i] + cssprop )
      if (css3propcamel.substr(0,2) == 'Ms') // if property starts with 'Ms'
        css3propcamel = 'm' + css3propcamel.substr(1) // Convert 'M' to lowercase
      if (css3propcamel in root.style)
        return css3propcamel
    }
    return undefined
  },
};
