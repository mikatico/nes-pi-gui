const classList = document.documentElement.classList;

export function hasClass(elem, className) {
  if (classList) {
    return elem.classList.contains(className);
  }
  return new RegExp('(^|\\s)' + className + '(\\s|$)').test(elem.className);
}

export function addClass(elem, className) {
  if (!this.hasClass(elem, className)) {
    if (classList) {
      elem.classList.add(className);
    }
    else {
      elem.className += (elem.className ? ' ' : '') + className;
    }
  }
}

export function removeClass(elem, className) {
  if (this.hasClass(elem, className)) {
    if (classList) {
      elem.classList.remove(className);
    }
    else {
      elem.className = elem.className.replace(new RegExp('(^|\\s)*' + className + '(\\s|$)*', 'g'), '');
    }
  }
}

export function toggleClass(elem, className) {
  if (classList) {
    elem.classList.toggle(className);
  }
  else {
    if (this.hasClass(elem, className)) { // eslint-disable-line
      elem.removeClass(className);
    }
    else {
      elem.addClass(className);
    }
  }
}
