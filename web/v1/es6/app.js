import './ui/slide';
import './config/constants';
import Nes from './nespi';

document.addEventListener('DOMContentLoaded', () => {
  const nes = new Nes();

  window.nes = nes;
});
