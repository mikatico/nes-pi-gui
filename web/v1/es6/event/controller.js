import Const from '../config/constants';

export function move(direction) {
  console.log('move ' + direction);
  switch (direction) {
    case Const.UP:
      console.log(direction);
      break;
    case Const.DOWN:
      console.log(direction);
      break;
    case Const.LEFT:
      console.log(direction);
      break;
    case Const.RIGHT:
      console.log(direction);
      break;
    default:
      console.log(direction);
      break;
  }
}

export function action(btn) {
  console.log('click ' + btn);
  switch (btn) {
    case Const.START:
      console.log(btn);
      break;
    case Const.SELECT:
      console.log(btn);
      break;
    case Const.A:
      console.log(btn);
      break;
    case Const.B:
      console.log(btn);
      break;
    default:
      console.log(btn);
      break;
  }
}
