import * as C from './dom/classes';
import Navigation from './helper/navigation';
import Client from './helper/client';
import './helper/keyboard';
import './helper/gamepad';

window.Const = Const;

class Nespi {
  constructor() {
    this.applicationName = 'NES PI';
    this.navigation = new Navigation(0, 1);
    this.client = Client;
    this.btnNext = document.querySelector('.next');
    this.btnPrev = document.querySelector('.prev');

    this.initializeEvent();
  }

  initializeEvent() {
    this.btnNext.addEventListener('click', () => {
      this.slideNext();
    });

    this.btnPrev.addEventListener('click', () => {
      this.slidePrev();
    });
  }

  getName() {
    return this.applicationName;
  }

  getNavigation() {
    return this.navigation;
  }

  move(direction) {
    const currentPositionY = this.navigation.getCurrentPositionY();
    const currentPositionX = this.navigation.getCurrentPositionX();
    switch (direction) {
      case Const.UP:
        switch(currentPositionY) {
          case 0:
            break;
          case 1:
            this.navigation.prevY();
            break;
          case 2:
            this.navigation.prevY();
            break;
          default:
            break;
        }
        break;
      case Const.DOWN:
        switch(currentPositionY) {
          case 0:
            this.navigation.nextY();
            break;
          case 1:
            this.navigation.nextY();
            break;
          case 2:
            break;
          default:
            break;
        }
        break;
      case Const.LEFT:
        switch(currentPositionY) {
          case 0:
            break;
          case 1:
            this.slidePrev();
            this.navigation.refreshCard();
            break;
          case 2:
            if (currentPositionX == 1) {
              this.navigation.prevX();
            }
            break;
          default:
            break;
        }
        break;
      case Const.RIGHT:
        switch(currentPositionY) {
          case 0:
            break;
          case 1:
            this.slideNext();
            this.navigation.refreshCard();
            this.navigation.focusCard();
            break;
          case 2:
            if (currentPositionX == 0) {
              this.navigation.nextX();
            }
            break;
          default:
            break;
        }
        break;
      default:
        break;
    }
  }

  action(btn) {
    console.log('click ' + btn);
    switch (btn) {
      case Const.START:
        console.log(btn);
        break;
      case Const.SELECT:
        console.log(btn);
        break;
      case Const.A:
        let current = this.navigation.getCurrent();
        this.execute(current);
        break;
      case Const.B:
        console.log(btn);
        break;
      default:
        console.log(btn);
        break;
    }
  }

  execute(obj) {
    // var modal = document.getElementById('myModal');
    // modal.style.display = "block";
    const action = obj.getAttribute('action');
    switch (action) {
      case 'start-game':
        const rom = obj.getAttribute('rom'); // TODO MAKE A MODAL CONFIRM
        this.client.get('/v1/play/' + rom, (response) => {
          // TODO case ERROR FOR MESSAGE ALERT 
        });
        break;
      case 'show-profil':

        break;
      case 'show-parameters':

        break;
      case 'show-power-action':

        break;
      default:
        break;
    }
  }

  slideNext() {
    slide.trigger('next.owl.carousel');
  }

  slidePrev() {
    slide.trigger('prev.owl.carousel', [300]);
  }
}

export default Nespi;